from PyQt5.QtCore import QObject, QThread, pyqtSignal
import sys
from time import sleep
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, QRect, QAbstractTableModel
from PyQt5.QtWidgets import (
    QProgressBar,
    QApplication,
    QLineEdit,
    QLabel,
    QMainWindow,
    QPushButton,
    QRadioButton,
    QVBoxLayout,
    QHBoxLayout,
    QWidget,
    QButtonGroup,
    QFileDialog,
    QTableView,
)
import cv2
import numpy as np
import pandas as pd

from image_viewer import QImageViewer

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'


# Step 1: Create a worker class
class Worker(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    def __init__(self, file='', parent=None):
        QThread.__init__(self, parent)
        self.file = file
        self.dataframe = pd.DataFrame([])

    def run(self):
        """Long-running task."""
        img = cv2.imread(self.file, 0)
        thresh, img_bin = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        img_bin = 255 - img_bin
        kernel_len = np.array(img).shape[1] // 100
        ver_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_len))
        hor_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_len, 1))
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
        image_1 = cv2.erode(img_bin, ver_kernel, iterations=3)
        vertical_lines = cv2.dilate(image_1, ver_kernel, iterations=3)
        image_2 = cv2.erode(img_bin, hor_kernel, iterations=3)
        horizontal_lines = cv2.dilate(image_2, hor_kernel, iterations=3)
        img_vh = cv2.addWeighted(vertical_lines, 0.5, horizontal_lines, 0.5, 0.0)
        # Eroding and thesholding the image
        img_vh = cv2.erode(~img_vh, kernel, iterations=2)
        thresh, img_vh = cv2.threshold(img_vh, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        bitxor = cv2.bitwise_xor(img, img_vh)
        bitnot = cv2.bitwise_not(bitxor)

        # Detect contours for following box detection
        contours, hierarchy = cv2.findContours(img_vh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours, boundingBoxes = self.sort_contours(contours, method="top-to-bottom")
        heights = [boundingBoxes[i][3] for i in range(len(boundingBoxes))]
        mean = np.mean(heights)

        # Create list box to store all boxes in
        box = []
        # Get position (x,y), width and height for every contour and show the contour on image
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            if w < 1000 and h < 500:
                box.append([x, y, w, h])

        row = []
        column = []

        for i in range(len(box)):
            if i == 0:
                column.append(box[i])
                previous = box[i]

            else:
                if box[i][1] <= previous[1] + mean / 2:
                    column.append(box[i])
                    previous = box[i]

                    if i == len(box) - 1:
                        row.append(column)

                else:
                    row.append(column)
                    column = []
                    previous = box[i]
                    column.append(box[i])

        countcol = 0
        for i in range(len(row)):
            countcol = len(row[i])
            if countcol > countcol:
                countcol = countcol

        # Retrieving the center of each column
        center = [int(row[i][j][0] + row[i][j][2] / 2) for j in range(len(row[i])) if row[0]]
        center = np.array(center)
        center.sort()

        finalboxes = []
        for i in range(len(row)):
            lis = []
            for k in range(countcol):
                lis.append([])
            for j in range(len(row[i])):
                diff = abs(center - (row[i][j][0] + row[i][j][2] / 4))
                minimum = min(diff)
                indexing = list(diff).index(minimum)
                lis[indexing].append(row[i][j])
            finalboxes.append(lis)

        outer = []
        for i in range(len(finalboxes)):
            for j in range(len(finalboxes[i])):
                inner = ''
                if len(finalboxes[i][j]) == 0:
                    outer.append(' ')
                else:
                    for k in range(len(finalboxes[i][j])):
                        y, x, w, h = finalboxes[i][j][k][0], finalboxes[i][j][k][1], finalboxes[i][j][k][2], \
                                     finalboxes[i][j][k][3]
                        finalimg = bitnot[x:x + h, y:y + w]
                        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 1))
                        border = cv2.copyMakeBorder(finalimg, 2, 2, 2, 2, cv2.BORDER_CONSTANT, value=[255, 255])
                        resizing = cv2.resize(border, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
                        dilation = cv2.dilate(resizing, kernel, iterations=1)
                        erosion = cv2.erode(dilation, kernel, iterations=2)

                        # out = pytesseract.image_to_string(erosion, lang='eng',
                        #                                   config='--psm 6 --oem 3 -c tessedit_char_whitelist=0123456789.,-')
                        out = pytesseract.image_to_string(erosion, lang='eng+nld',
                                                          config='--psm 6')
                        if len(out) == 0:
                            out = pytesseract.image_to_string(erosion, config='--psm 3')
                        inner = inner + " " + out
                    outer.append(inner)
            self.progress.emit(int((i + 1) / len(finalboxes) * 100))

        arr = np.array(outer)
        dataframe = pd.DataFrame(arr.reshape(len(row), countcol))
        dataframe = dataframe.replace(r'\n', ' ', regex=True)
        dataframe = dataframe.applymap(lambda x: str(x)[:-1])
        dataframe.style.set_properties(align="left")

        self.finished.emit()
        self.dataframe = dataframe


    @staticmethod
    def sort_contours(cnts, method="left-to-right"):
        # initialize the reverse flag and sort index
        reverse = False
        i = 0
        # handle if we need to sort in reverse
        if method == "right-to-left" or method == "bottom-to-top":
            reverse = True
        # handle if we are sorting against the y-coordinate rather than
        # the x-coordinate of the bounding box
        if method == "top-to-bottom" or method == "bottom-to-top":
            i = 1
        boundingBoxes = [cv2.boundingRect(c) for c in cnts]
        (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                            key=lambda b: b[1][i], reverse=reverse))
        # return the list of sorted contours and bounding boxes
        return (cnts, boundingBoxes)


class pandasModel(QAbstractTableModel):

    def __init__(self, data):
        QAbstractTableModel.__init__(self)
        self._data = data

    def rowCount(self, parent=None):
        return self._data.shape[0]

    def columnCount(self, parnet=None):
        return self._data.shape[1]

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            if role == Qt.DisplayRole:
                return str(self._data.iloc[index.row(), index.column()])
        return None

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self._data.columns[col]
        return None


class Window(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.clicksCount = 0
        self.setupUi()

    def setupUi(self):
        self.setWindowTitle("Image to table")
        self.resize(1600, 900)
        self.centralWidget = QWidget()
        self.setCentralWidget(self.centralWidget)
        # Create and connect widgets
        self.labelFile = QLabel('Geen file geselecteerd.')
        self.btnFile = QPushButton('Select image', self)
        self.btnFile.clicked.connect(self.select_file)
        self.dialog = QFileDialog(self)
        self.dialog.setViewMode(QFileDialog.Detail)
        self.dialog.setNameFilters(["Images (*.png *.jpg *.PNG *.JPG)"])
        self.dialog.selectNameFilter("Images (*.png *.jpg *.PNG *.JPG)")

        self.pic = QLabel(self)

        self.menuBtns = QHBoxLayout(self)
        self.menu = QButtonGroup(self)
        self.btnDetect = QRadioButton('Detect', self)
        self.btnTable = QRadioButton('Table', self)
        self.btnText = QRadioButton('Text', self)
        self.menu.addButton(self.btnDetect)
        self.menu.addButton(self.btnTable)
        self.menu.addButton(self.btnText)
        self.menuBtns.addWidget(self.btnDetect)
        self.menuBtns.addWidget(self.btnTable)
        self.menuBtns.addWidget(self.btnText)

        self.btnDetect.setChecked(True)
        self.mode = self.btnDetect.text()


        self.table = QTableView(self)

        self.clicksLabel = QLabel("", self)
        self.clicksLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        # self.stepLabel = QLabel("Progress: 0")
        # self.stepLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        # self.countBtn = QPushButton("Process image", self)
        # self.countBtn.clicked.connect(self.loadImage)
        self.longRunningBtn = QPushButton("Process image", self)
        self.longRunningBtn.clicked.connect(self.runLongTask)

        self.dialog_output = QFileDialog(self)
        self.dialog_output.setFileMode(QFileDialog.Directory)
        self.dialog_output.setViewMode(QFileDialog.Detail)
        self.folder = QPushButton("Select output folder", self)
        self.folder.clicked.connect(self.select_folder)
        self.labelFolder = QLabel('Geen folder geselecteerd.')
        self.excelName = QLineEdit(self)
        self.excelName.setText('data')
        self.toExcel = QPushButton("Export to Excel", self)
        self.toExcel.clicked.connect(self.to_excel)
        self.toExcel.setEnabled(False)
        self.folder.setEnabled(False)
        self.longRunningBtn.setEnabled(False)

        self.progress = QProgressBar(self)
        self.progress.setGeometry(200, 80, 250, 20)

        # Set the layout
        layout = QVBoxLayout()
        layout.addWidget(self.labelFile)
        layout.addWidget(self.btnFile)

        # layout.addWidget(self.clicksLabel)
        # layout.addWidget(self.countBtn)

        # layout.addStretch()
        layout.addLayout(self.menuBtns)

        layout.addWidget(self.longRunningBtn)
        layout.addWidget(self.progress)
        layout.addWidget(self.table)
        layout.addWidget(self.folder)
        layout.addWidget(self.labelFolder)
        layout.addWidget(self.excelName)
        layout.addWidget(self.toExcel)
        self.centralWidget.setLayout(layout)

        self.menu.buttonClicked[int].connect(self.slot)

    def slot(self, id):
        for button in self.menu.buttons():
            if button is self.menu.button(id):
                self.mode = button.text()

    def select_file(self):
        if self.dialog.exec_():
            self.folder_name = self.dialog.selectedFiles()[0]
            self.labelFile.setText(self.folder_name)
            self.excelName.setText('data_' + self.folder_name.split('/')[-1].split('.')[0])
            self.loadImage()
            # self.toExcel.setEnabled(True)
            self.longRunningBtn.setEnabled(True)

    def select_folder(self):
        if self.dialog_output.exec_():
            self.folderName = self.dialog_output.selectedFiles()[0]
            self.labelFolder.setText(self.folderName)
            self.toExcel.setEnabled(True)

    def to_excel(self):
        print('exporting')
        self.worker.dataframe.to_excel(self.folderName + '/' + self.excelName.text() + '.xlsx')

    def loadImage(self):
        # pixmap = QPixmap(self.folder_name)
        # self.pic.setPixmap(pixmap)
        # self.pic.setGeometry(QRect(self.width()//20, self.height()//10, self.height()//3, self.height()//3))  # (x, y, width, height)
        # self.pic.setPixmap(pixmap.scaled(self.pic.size()))
        # self.pic.show()
        # appIm = QApplication(sys.argv)
        imageViewer = QImageViewer(self, self.folder_name)
        # imageViewer.setWindowModality(Qt.ApplicationModal)
        imageViewer.show()
        # sys.exit(appIm.exec_())

    def reportProgress(self, n):
        # self.stepLabel.setText(f"Progress: {n}%")
        self.progress.setValue(n)

    def runLongTask(self):
        # Step 2: Create a QThread object
        self.thread = QThread()
        # Step 3: Create a worker object
        self.worker = Worker(file=self.folder_name)
        # Step 4: Move worker to the thread
        self.worker.moveToThread(self.thread)
        # Step 5: Connect signals and slots
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.worker.progress.connect(self.reportProgress)
        # Step 6: Start the thread
        self.thread.start()

        # Final resets
        # self.longRunningBtn.setEnabled(False)
        self.thread.finished.connect(
            lambda: self.longRunningBtn.setEnabled(True)
        )
        self.thread.finished.connect(
            lambda: self.folder.setEnabled(True)
        )
        # self.thread.finished.connect(
        #     lambda: self.stepLabel.setText("Progress: 0")
        # )

        self.thread.finished.connect(
            lambda: self.updateTable(self.worker.dataframe)
        )

        # self.thread.finished.connect(self.updateTable(self.worker.dataframe))

    def updateTable(self, data):
        model = pandasModel(data)
        self.table.setModel(model)

    @staticmethod
    def sort_contours(cnts, method="left-to-right"):
            # initialize the reverse flag and sort index
            reverse = False
            i = 0
            # handle if we need to sort in reverse
            if method == "right-to-left" or method == "bottom-to-top":
                reverse = True
            # handle if we are sorting against the y-coordinate rather than
            # the x-coordinate of the bounding box
            if method == "top-to-bottom" or method == "bottom-to-top":
                i = 1
            boundingBoxes = [cv2.boundingRect(c) for c in cnts]
            (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                                key=lambda b: b[1][i], reverse=reverse))
            # return the list of sorted contours and bounding boxes
            return (cnts, boundingBoxes)


app = QApplication(sys.argv)
win = Window()
win.show()
sys.exit(app.exec())
